f = open('input.txt')
lst = f.readlines()
m = 0
s = 0

def found_space(I,V):
    for j in range(I, len(lst)):
        q = int(lst[j])
        if q >= V:
            return j,V
    return found_space(I,V-1)

def calc_space(I,J,V):
    s = 0
    for i in lst[I:J]:
        s+= V - int(i)
    return s
i = 0
while i <= len(lst)-1:
    v = int(lst[i].replace('\n',''))
    if v < m:
        j,v = found_space(i,m)
        if j > i: s += calc_space(i,j,v)
        i = j
        m = v = int(lst[j])
    else:
        m = v
        i+=1
print(s)
